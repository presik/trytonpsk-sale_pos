# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import DeactivableMixin, ModelSQL, ModelView, fields
from trytond.modules.company.model import CompanyValueMixin
from trytond.modules.product import price_digits
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If
from trytond.transaction import Transaction

from .exceptions import ProductMixRequiredError


def round_num(value):
    return Decimal(str(round(value, 4)))


class Configuration(metaclass=PoolMeta):
    __name__ = 'product.configuration'

    code_required = fields.MultiValue(
        fields.Boolean(
            "Code Required",
            help="If code required and product dont have secuence and is good can not duplicate product"))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in 'code_required':
            return pool.get('account.configuration.code_required')
        return super().multivalue_model(field)


class ConfigurationCodeRequired(ModelSQL, CompanyValueMixin):
    "Account Configuration Code Required"
    __name__ = 'account.configuration.code_required'

    code_required = fields.Boolean("Default Code Required")


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    extra_tax = fields.Numeric('Extra Tax', digits=(16, 2))
    last_cost = fields.Numeric('Last Cost', digits=price_digits,
        help='Last purchase price')
    sale_price = fields.Function(fields.Numeric('Sale Price',
        digits=(16, 4)), 'get_sale_price_taxed')
    sale_price_taxed = fields.Function(fields.Numeric('Sale Price Taxed',
        digits=(16, 4)), 'get_sale_price_taxed')
    cost_price_taxed = fields.Function(fields.Numeric('Cost Price Taxed',
        digits=(16, 4)), 'get_cost_price_taxed')
    products_mix = fields.One2Many('product.product-mix.option',
        'parent_product', 'Mix')

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        super().__register__(module_name)
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('quantity_mix_required'):
            query = """UPDATE product_template
                SET quantity_mix_required = subquery.quantity_mix_required
                FROM (
                    SELECT DISTINCT ON (template) template, quantity_mix_required
                    FROM product_product
                    ORDER BY template, id
                ) subquery
                WHERE product_template.id = subquery.template"""
            cursor.execute(query)
            table_h.drop_column('quantity_mix_required')

    @property
    def products_mix_used(self):
        return self.products_mix or self.template.products_mix

    @classmethod
    def get_prices_taxed(cls, id_, cost_price, sale_price, sale_taxes):
        product, = cls.browse([id_])
        cost_price = product.get_cost_price_taxed(value=cost_price)
        if sale_taxes:
            sale_price = product.get_sale_price_taxed(value=sale_price)
        return cost_price, sale_price

    def get_cost_price_taxed(self, name=None, value=None):
        res = value if value is not None else (self.cost_price)
        tax_amount = 0
        if self.account_category:
            taxes_used = self.account_category.supplier_taxes_used
            taxes_used = [t for t in taxes_used if (t.type == 'percentage' and t.rate >= 0) or t.type == 'fixed']
            Tax = Pool().get('account.tax')
            Date = Pool().get('ir.date')
            today = Date.today()
            tax_list = Tax.compute(taxes_used, res, 1, today)
            extra_tax = self.extra_tax or 0
            tax_amount = sum([t['amount'] for t in tax_list], Decimal('0.0')) + extra_tax
        res = res + tax_amount
        return round_num(res)

    def get_sale_price_taxed(self, name=None, value=None, uom=None):
        pool = Pool()
        Tax = pool.get('account.tax')
        Date = pool.get('ir.date')
        today = Date.today()
        UOM = pool.get('product.uom')
        list_price = value or self.list_price or 0
        if self.salable and self.default_uom.id != self.sale_uom.id:
            list_price = UOM.compute_price(
                self.default_uom, list_price, self.sale_uom)
        if name == 'sale_price':
            return list_price

        tax_amount = 0
        if self.account_category:
            tax_list = Tax.compute(
                self.account_category.customer_taxes_used, list_price, 1, today)
            extra_tax = self.extra_tax or 0
            tax_amount = sum(
                    [t['amount'] for t in tax_list], Decimal('0.0'),
                ) + extra_tax
        res = list_price + tax_amount
        return round_num(res)


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    products_mix = fields.One2Many('product.product-mix.option',
        'parent_template', 'Mix')
    type_mix = fields.Selection([
            ('', ''),
            ('distributed', 'Distribuited'),
            ('choise', 'Choise'),
        ], 'Type Mix')

    quantity_mix_required = fields.Integer('Quantity Mix Required',
        states={'invisible': Eval('type_mix') != 'choise'})

    @fields.depends('products_mix', 'quantity_mix_required')
    def on_change_quantity_mix_required(self, name=None):
        if self.products_mix and self.quantity_mix_required and len(self.products_mix) < self.quantity_mix_required:
            raise ProductMixRequiredError(
                gettext(
                    "sale_pos_frontend_rest.msg_quantity_mix_error",
                    product_mix=len(self.products_mix),
                    quantity=self.quantity_mix_required))

    @classmethod
    def default_code_readonly(cls):
        return False

    def pre_validate(self):
        super().pre_validate()
        pool = Pool()
        Configuration = pool.get('product.configuration')
        required = False
        with Transaction().set_context(self._context):
            config = Configuration(1)
            required = config.get_multivalue('code_required')
        if self.type == 'goods' and self.salable and not self.code and required:
            raise UserError(gettext('sale_pos.msg_missing_code_product'))


class ProductMixOption(ModelSQL, ModelView):
    "Product Mix Option"
    __name__ = 'product.product-mix.option'
    parent_template = fields.Many2One('product.template', 'Parent Template',
        domain=[
            If(Bool(Eval('parent_product')),
                ('products', '=', Eval('parent_product')),
                ()),
            ],
        ondelete='CASCADE', required=True)
    parent_product = fields.Many2One('product.product', 'Parent Product',
        domain=[
            If(Bool(Eval('parent_template')),
                ('template', '=', Eval('parent_template')),
                ()),
            ],
        ondelete='CASCADE', required=True)
    option = fields.Many2One('product.product', 'Product',
        ondelete='CASCADE', required=True)
    product_unit_category = fields.Function(
        fields.Many2One('product.uom.category', "Product Unit Category"),
        'on_change_with_option_unit_category')
    quantity = fields.Float("Quantity", digits='unit', required=True)
    unit = fields.Many2One('product.uom', "Unit", required=True,
        domain=[
            If(Bool(Eval('product_unit_category')),
                ('category', '=', Eval('product_unit_category')),
                ('category', '!=', -1)),
            ],
        depends={'option'})
    list_price = fields.Numeric(
        "List Price", digits=price_digits,
        states={
            'readonly': ~Eval('context', {}).get('company'),
            })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        cursor = Transaction().connection.cursor()
        if table_h.column_exist('product'):
            table_h.column_rename('product', 'parent_product')
        super().__register__(module_name)
        query = """UPDATE "product_product-mix_option"
                SET parent_template = p.template
                FROM product_product as p
                WHERE "product_product-mix_option".parent_product = p.id"""
        cursor.execute(query)
        query = """UPDATE "product_product-mix_option"
                SET unit = t.default_uom
                FROM product_template as t
                WHERE "product_product-mix_option".parent_template = t.id"""
        cursor.execute(query)
        table_h.drop_column('quantity_mix_required')

    @property
    def parent_uom(self):
        if self.parent_product:
            return self.parent_product.default_uom
        elif self.parent_template:
            return self.parent_template.default_uom

    @classmethod
    def default_quantity(cls):
        return 1

    @classmethod
    def default_list_price(cls):
        return 0

    @fields.depends('option', 'unit', 'quantity', 'parent_template',
        '_parent_parent_template.type_mix',
        methods=['on_change_with_option_unit_category'])
    def on_change_option(self):
        if self.option:
            self.product_unit_category = (
                self.on_change_with_option_unit_category())
            if (not self.unit
                    or self.unit.category != self.product_unit_category):
                self.unit = self.option.default_uom
            if self.parent_template.type_mix == 'distributed':
                self.list_price = self.option.list_price

    @fields.depends('option')
    def on_change_with_option_unit_category(self, name=None):
        return self.option.default_uom.category if self.option else None

    @fields.depends(
        'parent_product', '_parent_parent_product.template')
    def on_change_parent_product(self):
        if self.parent_product:
            self.parent_template = self.parent_product.template

    def get_line(self, Line, quantity, unit, **values):
        pool = Pool()
        Uom = pool.get('product.uom')
        line = Line(product=self.option, **values)
        line.unit = self.unit
        quantity = Uom.compute_qty(
            unit, quantity, self.parent_uom, round=False)
        line.quantity = self.unit.round(quantity * self.quantity)
        line.base_price = self.list_price
        line.unit_price = self.list_price
        return line


class Category(DeactivableMixin, metaclass=PoolMeta):
    __name__ = 'product.category'
