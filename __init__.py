# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool

from . import account, device, invoice, ir, product, sale, shop, statement, user


def register():
    Pool.register(
        invoice.Invoice,
        product.Template,
        product.Configuration,
        product.ConfigurationCodeRequired,
        product.Category,
        device.SaleDevice,
        user.User,
        statement.Journal,
        sale.Sale,
        sale.SaleLine,
        statement.Statement,
        statement.StatementLine,
        product.Product,
        product.ProductMixOption,
        shop.SaleShop,
        shop.ShopDailySummaryStart,
        shop.ShopDailyCategoryStart,
        shop.SaleShopDiscounts,
        shop.SaleDiscount,
        sale.SaleUpdateDateStart,
        sale.SaleIncomeDailyStart,
        sale.SaleByKindStart,
        sale.PortfolioPaymentsStart,
        sale.SalesCostsStart,
        sale.SalesAuditStart,
        sale.SalePaymentForm,
        account.SaleAccountMovesStart,
        device.SaleDeviceStatementJournal,
        ir.Cron,
        module='sale_pos', type_='model')
    Pool.register(
        account.SaleAccountMovesReport,
        # shop.ShopDailySummaryReport,
        shop.ShopDailyCategoryReport,
        # sale.SaleDetailedReport,
        sale.SaleIncomeDailyReport,
        sale.SaleByKindReport,
        sale.PortfolioPaymentsReport,
        sale.SalesCostsReport,
        sale.SalesAuditReport,
        module='sale_pos', type_='report')
    Pool.register(
        account.SaleAccountMoves,
        sale.DeleteSalesDraft,
        sale.SaleUpdateDate,
        sale.SalesCosts,
        shop.ShopDailyCategory,
        sale.SaleIncomeDaily,
        sale.SaleByKind,
        sale.PortfolioPayments,
        sale.WizardSalePayment,
        sale.SalesAudit,
        sale.WorkflowReconcileSale,
        module='sale_pos', type_='wizard')
