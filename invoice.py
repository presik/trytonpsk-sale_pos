# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.i18n import gettext
from trytond.model import ModelView, Workflow, fields
from trytond.model.exceptions import AccessError
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval


class Invoice(metaclass=PoolMeta):
    STATES = {
        'invisible': Eval('type') != 'out',
        'readonly': Eval('state').in_(['paid', 'posted']),
    }
    __name__ = 'account.invoice'
    sale_device = fields.Many2One('sale.device', 'Sale Device',
        domain=[('shop', '=', Eval('shop'))], depends=['shop'], states=STATES)
    position = fields.Char('Position', states=STATES)
    turn = fields.Integer('Turn', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls.party.states['readonly'] = Bool(Eval('number'))
        if 'turn' not in cls._check_modify_exclude:
            cls._check_modify_exclude.add('turn')

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, invoices):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')

        cancel_moves = []
        delete_moves = []
        to_save = []
        for invoice in invoices:
            if invoice.move:
                if invoice.move.state == 'draft':
                    delete_moves.append(invoice.move)
                elif not invoice.cancel_move:
                    if (invoice.type == 'out'
                            and not invoice.company.cancel_invoice_out):
                        raise AccessError(
                            gettext('account_invoice.msg_invoice_customer_cancel_move',
                                invoice=invoice.rec_name))
                    invoice.cancel_move = invoice.move.cancel()
                    to_save.append(invoice)
                    cancel_moves.append(invoice.cancel_move)
            if invoice.move_charge and invoice.move_charge.state == 'draft':
                delete_moves.append(invoice.move_charge)
        if cancel_moves:
            Move.save(cancel_moves)
        cls.save(to_save)
        if delete_moves:
            Move.delete(delete_moves)
        if cancel_moves:
            Move.post(cancel_moves)
        # Write state before reconcile to prevent invoice to go to paid state
        cls.write(invoices, {
                'state': 'cancelled',
                })

        for invoice in invoices:
            if not invoice.move or not invoice.cancel_move:
                continue
            to_reconcile = []
            for line in invoice.move.lines + invoice.cancel_move.lines:
                if line.account == invoice.account:
                    to_reconcile.append(line)
            Line.reconcile(to_reconcile)

        cls._clean_payments(invoices)

    @classmethod
    def validate_invoice(cls, invoices):
        cls.validate_sequences([v for v in invoices if v.type == 'out' and not v.number and v.sales])
        super(Invoice, cls).validate_invoice(invoices)
        cls.set_invoice_number_for_sales(invoices)

    @classmethod
    def validate_sequences(cls, invoices):
        to_write = []
        values = []
        for v in invoices:
            for s in v.sales:
                if s.invoice_number and v not in values and s.invoice_method == 'order':
                    to_write.append([v])
                    to_write.append({'number': s.invoice_number})
                    values.append(v)
        if to_write:
            cls.write(*to_write)

    @classmethod
    def set_invoice_number_for_sales(cls, invoices):
        to_write = []
        for v in invoices:
            if v.type == 'out' and v.sales and not all([s.invoice_number for s in v.sales if s.invoice_method == 'order']):
                to_write.append(list(v.sales))
                to_write.append({
                    'invoice_number': v.number,
                    'invoice_date': v.invoice_date})
        if to_write:
            Sale = Pool().get('sale.sale')
            Sale.write(*to_write)
