# This file is part of the sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal

from trytond.model import fields
from trytond.pool import Pool, PoolMeta

_ZERO = Decimal("0.0")


class Journal(metaclass=PoolMeta):
    __name__ = 'account.statement.journal'
    # IS THIS NECCESARY?
    devices = fields.One2Many('sale.device', 'journal', 'Devices')


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'
    users = fields.Function(fields.One2Many(
        'res.user', None, 'Users'),
        'get_users', setter='set_users', searcher='search_users')
    turn = fields.Integer('Turn', states={'readonly': True})
    # count_money = fields.One2Many(
    #     'sale_pos.money_count', 'statement', 'Count Money')
    sale_device = fields.Many2One('sale.device', 'Device', required=False)
    hash_uid = fields.Char('Hash UID', readonly=True)
    # total_money = fields.Function(
    #     fields.Numeric("Total Money"), 'get_total_money')
    # amount_difference = fields.Function(fields.Numeric("Amount Difference"),
    #         'get_amount_difference')
    # expenses_daily = fields.One2Many('sale_pos.expenses_daily', 'statement',
    #         'Expenses Daily')
    # additional_income = fields.One2Many('sale_pos.additional_income', 'statement',
    #         'Additional Income')
    # balance_next_shift = fields.Numeric('balance next shift', digits=(10, 2))
    # # TODO delete this field
    # salesman = fields.Many2One('company.employee', 'Salesman', states={
    #     'readonly': Eval('state').in_(['validated', 'cancel', 'posted']),
    # })
    # cashier = fields.Many2One('party.party', 'Cashier', states={
    #     'readonly': Eval('state').in_(['validated', 'cancel', 'posted']),
    # })

    # @classmethod
    # def __setup__(cls):
    #     super(Statement, cls).__setup__()
    #     cls._transitions |= set((
    #         ('posted', 'draft'),
    #     ))
    #     cls._buttons.update({
    #         'force_draft': {
    #             'invisible': Eval('state') != 'posted',
    #             'depends': ['state'],
    #         },
    #     })

    # @classmethod
    # def __register__(cls, module_name):
    #     cursor = Transaction().connection.cursor()
    #     super(Statement, cls).__register__(module_name)
    #     table_h = cls.__table_handler__(module_name)
    #     # Migration from 6.0.20: remove salesman
    #     if table_h.column_exist('salesman'):
    #         query = """UPDATE account_statement
    #             SET cashier = company_employee.party
    #             FROM company_employee
    #             WHERE
    #                 account_statement.cashier IS NULL
    #                 AND account_statement.salesman IS NOT NULL
    #                 AND company_employee.id = account_statement.salesman;"""
    #         cursor.execute(query)
    #         # table_h.drop_column('salesman') TODO for delete column salesman

    # @classmethod
    # def cron_autopost(cls):
    #     # Add automatic post for validate statement
    #     statements = cls.search([
    #         ('state', '=', 'validated'),
    #     ], order=[('date', 'ASC')])
    #     if statements:
    #         cls.post([statements[0]])

    @classmethod
    def get_users(cls, statements, names):
        return {'users':
                {s.id: [
                    u.id
                    for j in s.journal
                    for d in j.devices
                    for u in d.users
                ]} for s in statements}

    @classmethod
    def search_users(cls, name, clause):
        pool = Pool()
        Journal = pool.get('account.statement.journal')
        Device = pool.get('sale.device')
        DeviceJournal = pool.get('sale.device.account.statement.journal')
        User = pool.get('res.user')

        statement = cls.__table__()
        journal = Journal.__table__()
        device = Device.__table__()
        device_journal = DeviceJournal.__table__()
        user = User.__table__()

        query = statement.join(
                journal, condition=statement.journal == journal.id).join(
                device_journal,
                    condition=journal.id == device_journal.journal).join(
                device, condition=device_journal.device == device.id).join(
                user, condition=device.id == user.sale_device).select(
                statement.id,
                where=user.id == clause[2])
        return [('id', 'in', query)]

    @classmethod
    def create_move(cls, statements):
        """Create move for the statements and try to reconcile the lines.
        Returns the list of move, statement and lines
        """
        pool = Pool()
        Line = pool.get('account.statement.line')
        Move = pool.get('account.move')
        Invoice = pool.get('account.invoice')

        to_write = []
        moves = []
        for statement in statements:
            for line in statement.lines:
                if line.move:
                    continue
                if line.invoice and line.invoice.state in (
                        'draft', 'validated'):
                    if not line.invoice.invoice_date:
                        for sale in line.invoice.sales:
                            line.invoice.write(
                                [line.invoice], {
                                    'invoice_date': sale.sale_date},
                            )
                    Invoice.post([line.invoice])
                move = line._get_move2()
                Move.post([move])
                to_write.append([line])
                to_write.append({'move': move.id})

                moves.append(move)
                for mline in move.lines:
                    if mline.account == line.account:
                        line.reconcile([(mline, line)])
        if to_write:
            Line.write(*to_write)
        return moves


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    extra_amount = fields.Numeric('Extra', digits=(16, 2))
    net_amount = fields.Function(fields.Numeric('Net Amount', digits=(16, 2)),
        'get_net_amount')

    def get_net_amount(self, name=None):
        res = 0
        if self.extra_amount and self.amount:
            res = self.extra_amount + self.amount
        return res

    @staticmethod
    def default_extra_amount():
        return 0

    @classmethod
    def delete(cls, lines):
        for line in lines:
            sale = line.sale
            if sale and hasattr(sale, 'additional_paid_amount'):
                sale.additional_paid_amount = 0
                sale.save()
        super(StatementLine, cls).delete(lines)


# class BillMoney(ModelSQL, ModelView):
#     "Bill Money"
#     __name__ = 'sale_pos.bill_money'
#     value = fields.Integer('Value', required=True)

#     @classmethod
#     def __setup__(cls):
#         super(BillMoney, cls).__setup__()
