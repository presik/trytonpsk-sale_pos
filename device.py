# This file is part of the sale_payment module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond import backend
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval


class SaleDevice(ModelSQL, ModelView):
    "Sale Device"
    __name__ = 'sale.device'
    name = fields.Char('Device Name', required=True)
    code = fields.Char('Code')
    shop = fields.Many2One('sale.shop', 'Shop', required=True)
    company = fields.Function(fields.Many2One('company.company', 'Company'),
        'get_company', searcher='search_company')
    journals = fields.Many2Many('sale.device.account.statement.journal',
        'device', 'journal', 'Journals', depends=['company'],
        domain=[('company', '=', Eval('company'))],
        )
    journal = fields.Many2One('account.statement.journal', "Default Journal",
        ondelete='RESTRICT', depends=['journals'],
        domain=[('id', 'in', Eval('journals', []))],
        )
    users = fields.One2Many('res.user', 'sale_device', 'Users')
    port = fields.Char('Device ID Port')
    environment = fields.Selection([
        ('', ''),
        ('retail', 'Retail'),
        ('restaurant', 'Restaurant'),
        ], 'Environment')

    @classmethod
    def __register__(cls, module_name):

        old_table = 'sale_pos_device'
        if backend.TableHandler.table_exist(old_table):
            backend.TableHandler.table_rename(old_table, cls._table)

        super(SaleDevice, cls).__register__(module_name)

    @fields.depends('shop')
    def on_change_shop(self):
        self.company = self.shop.company.id if self.shop else None

    def get_company(self, name):
        return self.shop.company.id

    @classmethod
    def search_company(cls, name, clause):
        return [('shop.%s' % name,) + tuple(clause[1:])]


class SaleDeviceStatementJournal(ModelSQL):
    "Sale Device - Statement Journal"
    __name__ = 'sale.device.account.statement.journal'
    _table = 'sale_device_account_statement_journal'
    device = fields.Many2One('sale.device', 'Sale Device',
            ondelete='CASCADE', required=True)
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
            ondelete='RESTRICT', required=True)

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        old_table = 'sale_pos_device_account_statement_journal'
        if backend.TableHandler.table_exist(old_table):
            backend.TableHandler.table_rename(old_table, cls._table)

        old_column = 'pos_device'
        new_column = 'device'
        if table_h.column_exist(old_column):
            table_h.drop_fk(old_column)
            table_h.column_rename(old_column, new_column)

        super(SaleDeviceStatementJournal, cls).__register__(module_name)
